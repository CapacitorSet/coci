import sys

nonUniqueInitials = [] # The non-unique list of first letters
uniqueInitials = set() # The unique list of first letters
candidates = []        # The list of initials matching the criteria

sys.stdin.readline()            # Discard the first row
players = sys.stdin.readlines() # Then, read the next lines

for player in players:
    # Loop through all players and store their data
	initial = player[0]
	nonUniqueInitials.append(initial)
	uniqueInitials.add(initial)

for initial in uniqueInitials:
	# Discard initials with fewer than 4 matching players
	if nonUniqueInitials.count(initial) >= 5:
		candidates.append(initial)

if len(candidates) == 0: # If no iniials fit the criteria
	sys.stdout.write("PREDAJA")
else:
    candidates.sort() # Sort initials lexicographically
    for candidate in candidates:
		sys.stdout.write(candidate)