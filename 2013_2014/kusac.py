import sys

sausages, testers = sys.stdin.readline().split(" ")

sausages = float(sausages)
testers = float(testers)

sausage_per_tester = float(sausages / testers) # The amount of sausage assigned to each tester
sausageLength = float(sausages)                # The length of all sausages together
cuts = 0                                       # The number of cuts made so far

for tester in range(1, int(testers) + 1):
	# For each tester, take away sausage_per_tester sausages.
	sausageLength -= sausage_per_tester
	if sausageLength % 1: # Is the current sausage split (i.e. is the length not an integer: 6,5, for example)?
		# If so, that means we had to cut the sausage.
		cuts += 1

print cuts