import sys

def combinations(iterable, r):
    # combinations('ABCD', 2) --> AB AC AD BC BD CD
    # combinations(range(4), 3) --> 012 013 023 123
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = range(r)
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)

mass = []
value = []
maxMasses = []

N, K = sys.stdin.readline().split(" ")
N = int(N)
K = int(K)

for count in range(0, N):
    currentMass, currentValue = sys.stdin.readline().split(" ")
    mass.append(int(currentMass))
    value.append(int(currentValue))

for count in range(0, K):
    currentMaxMass = sys.stdin.readline()
    maxMasses.append(int(currentMaxMass))

maxMasses.sort()

####################################

candidates = []

for combination in combinations(range(0, N), K):
    discard = False
    maxMasses_copy = maxMasses[:]
    masses = []
    for count in range(0, K):
        currentCombination = combination[count]
        currentMass = mass[currentCombination]
        masses.append(currentMass)
    masses.sort()

    for currentMass in masses:
        if maxMasses.pop() < currentMass:
            discard = True

    if discard == False:
        candidates.append(combination)
    maxMasses = maxMasses_copy

maxValue = 0

for candidate in candidates:
    currentValue = 0
    for piece in candidate:
        currentValue += value[piece]
    if currentValue > maxValue:
        maxValue = currentValue

print maxValue